from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from databases import Database
import uvicorn
from typing import Optional
from pydantic import BaseModel, validator
from enum import Enum

app = FastAPI()


class DatabaseSingleton:
    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = Database("sqlite:///tasks.db")
        return cls._instance


database = DatabaseSingleton.get_instance()
tasks = []


class TaskStatus(str, Enum):
    todo = "To do"
    doing = "Doing"
    done = "Done"


class Task(BaseModel):
    id: Optional[int]
    title: str
    status: TaskStatus

    @validator("status")
    def validate_status(cls, status: str) -> str:
        if status not in TaskStatus.__members__.values():
            raise ValueError("Invalid task status")
        return status


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/tasks")
async def get_tasks():
    query = "SELECT * FROM tasks"
    results = await database.fetch_all(query)
    return results


@app.post("/tasks")
async def create_task(task: Task):
    query = "INSERT INTO tasks (id, title, status) VALUES (:id, :title, :status)"
    values = {"id": task.id, "title": task.title, "status": task.status}
    await database.execute(query, values)
    return {"message": "Task created successfully"}


@app.put("/tasks/{task_id}")
async def update_task(task_id: int, task: Task):
    query = "UPDATE tasks SET title = :title, status = :status WHERE id = :task_id"
    values = {"task_id": task_id, "title": task.title, "status": task.status}
    result = await database.execute(query, values)
    if not result:
        raise HTTPException(status_code=404, detail="Task not found")
    return {"message": "Task updated successfully"}


@app.delete("/tasks/{task_id}")
async def delete_task(task_id: int):
    query = "DELETE FROM tasks WHERE id = :task_id"
    values = {"task_id": task_id}
    result = await database.execute(query, values)
    if not result:
        raise HTTPException(status_code=404, detail="Task not found")
    return {"message": "Task deleted successfully"}
