from databases import Database

database = Database("sqlite:///tasks.db")

migration_script = """
CREATE TABLE IF NOT EXISTS tasks (
    id INTEGER PRIMARY KEY,
    title TEXT,
    status TEXT
);
"""


async def create_tasks_table():
    await database.connect()
    await database.execute(migration_script)
    await database.disconnect()


if __name__ == "__main__":
    import asyncio

    asyncio.run(create_tasks_table())
