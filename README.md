# Task Management API

The Task Management API is a FastAPI-based web application that provides endpoints for managing tasks. It utilizes various design patterns, such as Singleton, Observer, Decorator, and more, to demonstrate their usage and showcase different architectural approaches. This project was created as a learning exercise to gain a better understanding of design patterns and their practical applications in web development.

## Features

- Create, read, update, and delete tasks.
- Retrieve a list of tasks.
- Validate task status using an enum.
- Demonstrates the Singleton, Observer, and Decorator design patterns.
- Utilizes the FastAPI framework for building the API.

## Design Patterns

The following design patterns are implemented in this project:

- **Singleton**: Ensures a single instance of the database connection is used throughout the application.
- **Observer**: Manages the database connection and handles startup and shutdown events.
- **Decorator**: Wraps API endpoints with additional behavior, such as task status validation.
- **Factory**: Wraps API endpoints with additional behavior, such as task status validation.

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/task-management-api.git
   ```

2. Change into the project directory:

   ```bash
   cd task-management-api
   ```

2. Install dependencies:

   ```bash
   pip install -r requirements.txt
   ```

## Usage

To run the Task Management API, use the following command:

   ```bash
   python main.py [design_pattern]
   ```

Replace [design_pattern] with one of the following options:

- singleton - Run the application using the Singleton design pattern.
- observer - Run the application using the Observer design pattern.
- decorator - Run the application using the Decorator design pattern.

The API will be accessible at http://localhost:8000.


## API Endpoints

The following API endpoints are available:

- GET /tasks: Retrieve a list of tasks.=
- POST /tasks: Create a new task.
- PUT /tasks/{task_id}: Update an existing task.
- DELETE /tasks/{task_id}: Delete a task.

Refer to the API documentation or the code implementation for more details on request and response structures.


## Contributing
Contributions are welcome! If you'd like to contribute to this project, please follow these steps:

- Fork the repository.
- Create a new branch.
- Make your changes.
- Commit your changes and push to your forked repository.
- Submit a pull request.


Please ensure that your code follows the existing code style, includes appropriate tests, and provides clear documentation for your changes.


```
Feel free to copy this markdown text and use it to update your README.md file with the "Usage", "API Endpoints", and "Contributing" sections.
```
