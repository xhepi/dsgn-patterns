import pytest
from unittest.mock import MagicMock
import random
from fastapi.testclient import TestClient
from main import app
from databases import Database

client = TestClient(app)


@pytest.fixture(scope="module")
def test_database():
    # Create a separate test database file
    test_db_url = "sqlite:///test_tasks.db"
    database = Database(test_db_url)

    async def setup_database():
        await database.connect()
        query = """
        CREATE TABLE IF NOT EXISTS tasks (
            id INTEGER PRIMARY KEY,
            title TEXT,
            status TEXT
        );
        """
        await database.execute(query)

    async def shutdown_database():
        await database.disconnect()

    app.state.database = database

    # Perform necessary setup steps, such as creating tables or inserting test data
    app.add_event_handler("startup", setup_database)
    app.add_event_handler("shutdown", shutdown_database)

    yield  # Execute the tests

    # Clean up the test database after running the tests
    query = "DROP TABLE IF EXISTS tasks;"
    app.state.database.execute(query)


def test_get_tasks(test_database):
    query = "INSERT INTO tasks (title, status) VALUES ('Task 1', 'To do');"
    app.state.database.execute(query)

    response = client.get("/tasks")
    assert response.status_code == 200
    breakpoint()
    assert len(response.json()) > 0


def test_create_task(test_database):
    query = "DELETE FROM tasks;"
    app.state.database.execute(query)

    payload = {"title": "New Task", "status": "To do"}
    response = client.post("/tasks", json=payload)
    assert response.status_code == 200


def test_update_task(test_database):
    app.state.database.execute = MagicMock(return_value=None)

    app.state.database.fetch_all = MagicMock(return_value=[(1,)])

    random_id = random.choice(client.get("/tasks").json()).get("id")

    payload = {"title": "Updated Task", "status": "Doing"}
    response = client.put(f"/tasks/{random_id}", json=payload)
    assert response.status_code == 200


def test_delete_task(test_database):
    app.state.database.execute = MagicMock(return_value=None)

    app.state.database.fetch_all = MagicMock(return_value=[(1,)])

    random_id = random.choice(client.get("/tasks").json()).get("id")

    response = client.delete(f"/tasks/{random_id}")
    assert response.status_code == 200
