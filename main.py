import sys
import uvicorn

if len(sys.argv) != 2:
    print("Usage: python main.py [design_pattern]")
    sys.exit(1)

design_pattern = sys.argv[1]

if design_pattern == "singleton":
    from patterns.singleton import app
elif design_pattern == "observer":
    from patterns.observer import app
elif design_pattern == "decorator":
    from patterns.decorator import app
elif design_pattern == "factory":
    from patterns.factory import app
else:
    print("Invalid design pattern argument.")
    sys.exit(1)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
